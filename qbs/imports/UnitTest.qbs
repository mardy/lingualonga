import qbs

Test {
    property string classFileName: ""

    name: "tst_" + classFileName

    cpp.includePaths: [ project.sourceDirectory + "/src/" ]
    cpp.rpaths: cpp.libraryPaths

    Group {
        prefix: product.sourceDirectory + "/"
        files: [
            "../src/" + classFileName + ".cpp",
            "../src/" + classFileName + ".h",
            "tst_" + classFileName + ".cpp",
        ]
    }

    Depends { name: "buildconfig" }
}
