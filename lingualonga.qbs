import qbs 1.0

Project {
    name: "lingualonga"

    property bool buildTests: false
    property bool enableCoverage: false

    references: [
        "tests/tests.qbs",
    ]
    qbsSearchPaths: "qbs"

    QtGuiApplication {
        name: "it.mardy.lingualonga.editor"
        targetName: "lingualonga"
        install: true

        files: [
            "src/document_handler.cpp",
            "src/document_handler.h",
            "src/file_io.cpp",
            "src/file_io.h",
            "src/json_storage.cpp",
            "src/json_storage.h",
            "src/main.cpp",
            "src/standard_paths.cpp",
            "src/standard_paths.h",
            "src/temporary_dir.cpp",
            "src/temporary_dir.h",
            "src/utils.cpp",
            "src/utils.h",
        ]

        Group {
            files: [
                "data/qtquickcontrols2.conf",
                "src/qml/*.js",
                "src/qml/*.qml",
            ]
            fileTags: ["qt.core.resource_data"]
        }

        Group {
            files: [
                "data/fonts/fontello.ttf",
            ]
            Qt.core.resourceSourceBase: "data"
            fileTags: ["qt.core.resource_data"]
        }

        Depends { name: "Qt.quick" }
        Depends { name: "buildconfig" }

        Properties {
            condition: qbs.targetOS.contains("android")
            Android.sdk.sourceSetDir: product.sourceDirectory + "/src/editor"
        }
    }

    AutotestRunner {
        name: "check"
        environment: [ "QT_QPA_PLATFORM=offscreen" ]
        Depends { productTypes: ["coverage-clean"] }
    }
}
