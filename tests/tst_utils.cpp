/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"

#include <QDebug>
#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <QTemporaryDir>
#include <QTest>

using namespace LinguaLonga;

class UtilsTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testUniqueBaseName_data();
    void testUniqueBaseName();
    void testParseJson_data();
    void testParseJson();
    void testRenameDirectory_data();
    void testRenameDirectory();
};

void UtilsTest::testUniqueBaseName_data()
{
    QTest::addColumn<QString>("title");
    QTest::addColumn<QString>("extension");
    QTest::addColumn<QString>("expectedBaseName");

    QTest::newRow("capitalization") <<
        "One Two Three" <<
        ".pdf" <<
        "one-two-three";

    QTest::newRow("spaces") <<
        "One  Two   Three" <<
        ".pdf" <<
        "one-two-three";

    QTest::newRow("special characters") <<
        "One + Two, Three! Four\\?" <<
        ".pdf" <<
        "one--two-three-four";

    QTest::newRow("collision") <<
        "One two" <<
        ".jpg" <<
        "one-two1";

    QTest::newRow("no collision (different extension)") <<
        "One two" <<
        ".pdf" <<
        "one-two";

    QTest::newRow("repeated collision") <<
        "Hello, world!" <<
        ".pdf" <<
        "hello-world2";
}

void UtilsTest::testUniqueBaseName()
{
    QFETCH(QString, title);
    QFETCH(QString, extension);
    QFETCH(QString, expectedBaseName);

    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());

    /* Create some file to generate filename conflicts */
    QStringList fileNames {
        "one-two.jpg",
        "hello-world.pdf",
        "hello-world1.pdf",
    };
    for (const QString &fileName: fileNames) {
        QFile file(tmpDir.filePath(fileName));
        file.open(QIODevice::WriteOnly);
    }

    Utils utils;
    QString baseName = utils.uniqueBaseName(tmpDir.path(), title, extension);
    QCOMPARE(baseName, expectedBaseName);
}

void UtilsTest::testParseJson_data()
{
    QTest::addColumn<QString>("json");
    QTest::addColumn<QJsonValue>("defaultValue");
    QTest::addColumn<QJsonValue>("expectedResult");

    QTest::newRow("empty") <<
        "" <<
        QJsonValue(QJsonObject { { "key", "value" } }) <<
        QJsonValue(QJsonObject { { "key", "value" } });

    QTest::newRow("invalid") <<
        "{ I wish this was JSON, but it isn't! [[[}" <<
        QJsonValue(QJsonObject { { "key", "default" } }) <<
        QJsonValue(QJsonObject { { "key", "default" } });

    QTest::newRow("Object") <<
        R"({
            "number": 3.14,
            "text": "Hi!",
            "list": [ 1, 2, 3 ]
        })" <<
        QJsonValue(QJsonObject { { "key", "value" } }) <<
        QJsonValue(QJsonObject {
            { "number", 3.14 },
            { "text", "Hi!" },
            { "list", QJsonArray { 1, 2, 3 } },
        });

    QTest::newRow("Array") <<
        R"([ "some", "nice", "words" ])" <<
        QJsonValue(QJsonObject { { "key", "value" } }) <<
        QJsonValue(QJsonArray { "some", "nice", "words" });
}

void UtilsTest::testParseJson()
{
    QFETCH(QString, json);
    QFETCH(QJsonValue, defaultValue);
    QFETCH(QJsonValue, expectedResult);

    Utils utils;
    QJsonValue result = utils.parseJson(json.toUtf8(), defaultValue);
    QCOMPARE(result, expectedResult);
}

void UtilsTest::testRenameDirectory_data()
{
    QTest::addColumn<QString>("path");
    QTest::addColumn<QString>("name");

    QTest::newRow("simple") <<
        "some directory" <<
        "move-it-here";

    QTest::newRow("trailing slash") <<
        "origin/" <<
        "destination";
}

void UtilsTest::testRenameDirectory()
{
    QFETCH(QString, path);
    QFETCH(QString, name);

    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());

    QDir dir(tmpDir.path());
    dir.mkpath(path);
    QVERIFY(dir.cd(path));

    /* Create a file to verify that it's moved over */

    QString fileName = QStringLiteral("test-file.txt");
    QByteArray contents = "Hello, world!";
    {
        QFile file(dir.filePath(fileName));
        file.open(QIODevice::WriteOnly);
        file.write(contents);
    }

    /* Do the rename */
    Utils utils;
    QVERIFY(utils.renameDirectory(tmpDir.path() + "/" + path, name));

    /* Verify that the directory has been moved */
    dir.refresh();
    QVERIFY(!dir.exists());

    dir = QDir(tmpDir.filePath(name));
    QVERIFY(dir.exists());
    {
        QFile file(dir.filePath(fileName));
        QVERIFY(file.open(QIODevice::ReadOnly));
        QCOMPARE(file.readAll(), contents);
    }
}

QTEST_GUILESS_MAIN(UtilsTest)

#include "tst_utils.moc"
