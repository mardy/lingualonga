import qbs 1.0

Project {
    condition: project.buildTests

    UnitTest {
        classFileName: "file_io"
    }

    UnitTest {
        classFileName: "json_storage"
        Depends { name: 'Qt.qml' }
    }

    UnitTest {
        classFileName: "standard_paths"
    }

    UnitTest {
        classFileName: "temporary_dir"
    }

    UnitTest {
        classFileName: "utils"
    }

    CoverageClean {
        condition: project.enableCoverage
    }

    CoverageReport {
        condition: project.enableCoverage
        extractPatterns: [ '*/src/*.cpp', '*/lib/*.cpp' ]
    }
}
