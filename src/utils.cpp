/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"

#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJsonDocument>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonParseError>
#include <QRegularExpression>

using namespace LinguaLonga;

static QString composeName(const QString &name, int index)
{
    return index == 0 ? name : (name + QString::number(index));
}

Utils::Utils(QObject *parent):
    QObject(parent)
{
}

QString Utils::uniqueBaseName(const QString &path,
                              const QString &title,
                              const QString &extension) const
{
    QString baseName = title.simplified().toLower();
    baseName.replace(QRegularExpression("\\s"), "-");
    baseName.replace(QRegularExpression("[^a-zA-Z0-9-]"), "");

    int i = 0;
    QString name;
    while (name = composeName(baseName, i),
           QFile::exists(path + "/" + name + extension)) {
        i++;
    }
    return name;
}

QJsonValue Utils::parseJson(const QByteArray &json,
                            const QJsonValue &defaultValue) const
{
    QJsonValue ret;

    QJsonParseError error;
    QJsonDocument doc = QJsonDocument::fromJson(json, &error);
    if (error.error != QJsonParseError::NoError) {
        ret = defaultValue;
    } else if (doc.isObject()) {
        ret = doc.object();
    } else if (doc.isArray()) {
        ret = doc.array();
    }
    return ret;
}

bool Utils::renameDirectory(const QString &path, const QString &name) const
{
    QString stripped(path);
    while (stripped.endsWith('/')) {
        stripped.truncate(stripped.length() - 1);
    }

    QFileInfo fileInfo(stripped);
    return fileInfo.dir().rename(fileInfo.fileName(), name);
}
