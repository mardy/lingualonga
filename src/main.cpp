/*
 * Copyright (C) 2019-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "document_handler.h"
#include "file_io.h"
#include "json_storage.h"
#include "standard_paths.h"
#include "temporary_dir.h"
#include "utils.h"

#include <QDebug>
#include <QFont>
#include <QFontDatabase>
#include <QGuiApplication>
#include <QQmlApplicationEngine>

static QObject *utilsProvider(QQmlEngine *, QJSEngine *)
{
        return new LinguaLonga::Utils;
}

int main(int argc, char *argv[])
{
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QGuiApplication app(argc, argv);

    qmlRegisterType<LinguaLonga::DocumentHandler>("it.mardy.lingualonga",
                                                  1, 0, "DocumentHandler");
    qmlRegisterType<LinguaLonga::FileIO>("it.mardy.lingualonga",
                                         1, 0, "FileIO");
    qmlRegisterType<LinguaLonga::JsonStorage>("it.mardy.lingualonga",
                                              1, 0, "JsonStorage");
    qmlRegisterType<LinguaLonga::StandardPaths>("it.mardy.lingualonga",
                                                1, 0, "StandardPaths");
    qmlRegisterType<LinguaLonga::TemporaryDir>("it.mardy.lingualonga",
                                               1, 0, "TemporaryDir");
    qmlRegisterSingletonType<LinguaLonga::Utils>("it.mardy.lingualonga",
                                                 1, 0, "Utils",
                                                 utilsProvider);

    QFontDatabase fontDatabase;
    if (fontDatabase.addApplicationFont(":/fonts/fontello.ttf") == -1) {
        qWarning() << "Failed to load fontello.ttf";
    }
    QFont::insertSubstitutions("Corsivo", {
        "Coming Soon", // for Android
        "Purisa",
        "Comic Sans MS",
        });

    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/lingualonga.qml")));

    return app.exec();
}
