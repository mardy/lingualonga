/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINGUALONGA_STANDARD_PATHS_H
#define LINGUALONGA_STANDARD_PATHS_H

#include <QObject>
#include <QStandardPaths>
#include <QString>

namespace LinguaLonga {

class StandardPathsPrivate;
class StandardPaths: public QObject
{
    Q_OBJECT
    Q_PROPERTY(StandardLocation base READ base WRITE setBase \
               NOTIFY baseChanged)
    Q_PROPERTY(QString basePath READ basePath NOTIFY baseChanged)

public:
    enum StandardLocation {
        DesktopLocation = QStandardPaths::DesktopLocation,
        DocumentsLocation = QStandardPaths::DocumentsLocation,
        MusicLocation = QStandardPaths::MusicLocation,
        MoviesLocation = QStandardPaths::MoviesLocation,
        PicturesLocation = QStandardPaths::PicturesLocation,
        TempLocation = QStandardPaths::TempLocation,
        HomeLocation = QStandardPaths::HomeLocation,
        DataLocation = QStandardPaths::DataLocation,
        CacheLocation = QStandardPaths::CacheLocation,
        RuntimeLocation = QStandardPaths::RuntimeLocation,
        ConfigLocation = QStandardPaths::ConfigLocation,
        AppDataLocation = QStandardPaths::AppDataLocation,
        AppConfigLocation = QStandardPaths::AppConfigLocation,
    };
    Q_ENUM(StandardLocation)

    StandardPaths(QObject *parent = 0);
    ~StandardPaths();

    void setBase(StandardPaths::StandardLocation base);
    StandardPaths::StandardLocation base() const;
    QString basePath() const;

    Q_INVOKABLE QString filePath(const QString &relativePath) const;

Q_SIGNALS:
    void baseChanged();

private:
    QScopedPointer<StandardPathsPrivate> d_ptr;
    Q_DECLARE_PRIVATE(StandardPaths)
};

} // namespace

#endif // LINGUALONGA_STANDARD_PATHS_H
