import QtQuick.Controls 2.12
import it.mardy.lingualonga 1.0

Button {
    id: root

    property string parentPath: ""
    property string courseId: ""

    property var _course: Utils.parseJson(fileIO.contents, {})

    flat: true
    text: qsTr("%1 - %2").arg(index + 1).arg(_course.title)
    onClicked: {
        var item = stackView.push(Qt.resolvedUrl("CourseEditor.qml"), {
            "parentPath": root.parentPath,
            "courseId": root.courseId,
        })
        item.saved.connect(function() {
            // the title might have been changed, force a reload
            fileIO.reload()
        })
    }

    FileIO {
        id: fileIO
        filePath: root.parentPath + "/" + root.courseId + ".llc/index.ll"
    }
}

