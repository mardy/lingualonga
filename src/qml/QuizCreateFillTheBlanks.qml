import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ScrollView {
    id: root

    property var actions: [ actionMakeBlank, actionRemoveBlank ]
    property var quizData: ({})

    anchors.fill: parent
    contentWidth: width

    property string fillableText: quizData.fillableText || ""
    property var _blanks: quizData.blanks || []
    property string _startBlankSequence: "<b><font face=\"Corsivo\" color=\"blue\">"
    property string _endBlankSequence: "</font></b>"
    property string _formattedText: ""

    Component.onCompleted: _formattedText = formattedText(root.fillableText, root._blanks)

    ColumnLayout {
        anchors { fill: parent; leftMargin: 8; rightMargin: 8 }

        Label {
            Layout.fillWidth: true
            wrapMode: TextArea.Wrap
            text: qsTr("Enter the text, then select the parts that must be filled by the student and press the \"abc→_\" button on the toolbar")
        }

        TextArea {
            id: textArea
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.preferredHeight: 150
            placeholderText: qsTr("Type your text here")
            wrapMode: TextArea.Wrap
            horizontalAlignment: TextEdit.AlignJustify
            textFormat: Text.RichText
            selectByMouse: true
            persistentSelection: true
            text: root._formattedText
        }
    }

    Action {
        id: actionMakeBlank
        text: "abc→_"
        enabled: textArea.selectionEnd > textArea.selectionStart
        onTriggered: root.makeBlank(textArea.selectionStart, textArea.selectionEnd)
    }

    Action {
        id: actionRemoveBlank
        text: "_→abc"
        enabled: blankAt(textArea.cursorPosition) >= 0
        onTriggered: root.removeBlank(textArea.cursorPosition)
    }

    function formattedText(text, blanks) {
        var ret = ""
        var pos = 0
        for (var i = 0; i < blanks.length; i++) {
            var blank = blanks[i]
            ret += text.substring(pos, blank.start)
            var blankText = text.substring(blank.start, blank.end)
            ret += _startBlankSequence + blankText + _endBlankSequence
            pos = blank.end
        }
        ret += text.substring(pos)
        return ret
    }

    function makeBlank(start, end) {
        var removed = textArea.getText(start, end)
        textArea.remove(start, end)
        var ret = textArea.insert(start, _startBlankSequence + removed + _endBlankSequence)

        /* The highlighted text might contain extra spaces, which the TextArea
         * strips out when we re-insert it back. Therefore, we don't use `end`
         * but recalculate it.
         */
        removed = removed.replace(/\s+/g, ' ')
        addBlankToFillableText(start, start + removed.length)
    }

    function addBlankToFillableText(start, end) {
        var blanks = root._blanks
        var newBlanks = []
        var added = false
        for (var i = 0; i < blanks.length; i++) {
            var blank = blanks[i]
            if (start > blank.end) {
                newBlanks.push(blank)
            } else if (start > blank.start) {
                // merge with preceding
                newBlanks.push({ "start": blank.start, "end": end })
                added = true
            } else if (end < blank.start) {
                if (!added) {
                    newBlanks.push({ "start": start, "end": end })
                    added = true
                }
                newBlanks.push(blank)
            } else if (end <= blank.end) {
                // merge with following
                newBlanks.push({ "start": start, "end": blank.end })
                added = true
            }
        }
        if (!added) {
            newBlanks.push({ "start": start, "end": end })
        }
        root._blanks = newBlanks

        printBlanks()
    }

    function blankAt(pos) {
        for (var i = 0; i < _blanks.length; i++) {
            var blank = _blanks[i]
            if (pos < blank.start) break
            if (pos <= blank.end && pos >= blank.start) return i
        }
        return -1
    }

    function removeBlank(pos) {
        var i = blankAt(pos)
        var newBlanks = _blanks
        var blank = newBlanks.splice(i, 1)[0]
        _blanks = newBlanks

        var text = textArea.getText(blank.start, blank.end)
        textArea.remove(blank.start, blank.end)
        textArea.insert(blank.start, text)
    }

    function printBlanks() {
        for (var i = 0; i < _blanks.length; i++) {
            var blank = _blanks[i]
            console.log("Blank ("+blank.start+" - " + blank.end + "): '" + textArea.getText(blank.start, blank.end) + "'")
        }
    }

    function cleanRange(text, start, end) {
        var cleaned = text.substring(start, end).replace(/\s+/g, ' ')
        if (start == 0) cleaned = cleaned.replace(/^\s/, '')
        return cleaned
    }

    function cleanText(text) {
        var ret = ""
        var newBlanks = []
        var removedChars = 0
        var pos = 0
        var cleaned
        for (var i = 0; i < _blanks.length; i++) {
            var blank = _blanks[i]
            cleaned = cleanRange(text, pos, blank.start)
            ret += cleaned
            removedChars += blank.start - pos - cleaned.length
            var start = blank.start - removedChars
            cleaned = cleanRange(text, blank.start, blank.end)
            ret += cleaned
            removedChars += blank.end - blank.start - cleaned.length
            var end = blank.end - removedChars
            newBlanks.push({ "start": start, "end": end })
            pos = blank.end
        }
        cleaned = cleanRange(text, pos, text.lengh)
        ret += cleaned
        root._blanks = newBlanks
        return ret
    }

    function save() {
        console.log("save")
        var areaText = textArea.getText(0, textArea.length)
        var cleanedText = cleanText(areaText)
        textArea.text = formattedText(cleanedText, _blanks)
        var data = {
            "fillableText": cleanedText,
            "blanks": root._blanks,
        }
        root.quizData = data
        return true
    }
}
