import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ScrollView {
    id: root

    property var quizData: null

    contentWidth: width

    onQuizDataChanged: if (quizData && quizData.matches) columns.prepareModel(quizData.matches)

    ColumnLayout {
        anchors { fill: parent; leftMargin: 8; rightMargin: 8 }

        Label {
            Layout.fillWidth: true
            wrapMode: Text.WordWrap
            text: quizData.question
        }

        Item {
            id: columns
            Layout.fillWidth: true
            implicitWidth: columnsLayout.implicitWidth
            implicitHeight: columnsLayout.implicitHeight

            RowLayout {
                id: columnsLayout
                anchors.fill: parent

                ColumnLayout {
                    Layout.fillWidth: true

                    Repeater {
                        model: columnModel

                        MouseArea {
                            id: dragArea

                            Layout.fillWidth: true
                            property int modelIndex: index
                            property bool held: false
                            property bool hovering: false
                            property var color: model.color
                            implicitWidth: content.implicitWidth
                            implicitHeight: content.implicitHeight

                            drag.target: held ? content : undefined
                            drag.axis: Drag.XAndYAxis
                            onPressed: held = true
                            onReleased: { held = false; content.Drag.drop() }

                            ColoredLabel {
                                id: content
                                anchors {
                                    // fill won't work with AnchorChanges
                                    left: parent.left; right: parent.right
                                    top: parent.top; bottom: parent.bottom
                                }
                                color: model.targetIndex >= 0 ? dragArea.color : "transparent"
                                text: model.text1
                                borderColor: dragArea.hovering ? dragArea.color : "transparent"
                                states: State {
                                    when: dragArea.drag.active
                                    AnchorChanges {
                                        target: content
                                        anchors { left: undefined; right: undefined; top: undefined; bottom: undefined }
                                    }
                                    ParentChange { target: content; parent: columns }
                                }
                                Drag.active: dragArea.drag.active
                                Drag.source: dragArea
                                Drag.hotSpot.x: width / 2
                                Drag.hotSpot.y: height / 2
                            }
                        }
                    }
                }

                ColumnLayout {
                    Layout.fillWidth: true

                    Repeater {
                        id: column2
                        model: columnModel

                        ColoredLabel {
                            Layout.fillWidth: true
                            text: model.text2

                            DropArea {
                                id: dropArea
                                anchors.fill: parent
                                onEntered: {
                                    var sourceIndex = drag.source.modelIndex
                                    parent.borderColor = columnModel.get(sourceIndex).color
                                    drag.source.hovering = true
                                }
                                onExited: {
                                    parent.borderColor = "transparent"
                                    drag.source.hovering = false
                                }
                                onDropped: {
                                    var sourceIndex = drag.source.modelIndex
                                    parent.color = columnModel.get(sourceIndex).color
                                    columns.updateRow(sourceIndex, index)
                                    drag.source.hovering = false
                                }
                            }
                        }
                    }
                }
            }

            ListModel {
                id: columnModel
                dynamicRoles: true
            }

            function prepareModel(origModel) {
                var shuffledIndexes = [...Array(origModel.length).keys()];
                shuffleArray(shuffledIndexes)
                columnModel.clear()
                for (var i = 0; i < origModel.length; i++) {
                    var orig = origModel[i]
                    var color = Qt.hsla(i / origModel.length, 0.3, 0.7, 1)
                    var row = {
                        text1: orig.text1,
                        text2: origModel[shuffledIndexes[i]].text2,
                        color: color,
                        targetIndex: -1,
                    }
                    columnModel.append(row)
                }
            }

            function updateRow(index, targetIndex) {
                if (targetIndex >= 0) {
                    // Ensure that the same index is not already assigned
                    for (var i = 0; i < columnModel.count; i++) {
                        var rowTargetIndex = columnModel.get(i).targetIndex
                        if (rowTargetIndex == targetIndex) {
                            columnModel.setProperty(i, "targetIndex", -1)
                            break
                        }
                    }
                }
                var oldTargetIndex = columnModel.get(index).targetIndex
                if (oldTargetIndex >= 0) {
                    // clear the old target item
                    var item = column2.itemAt(oldTargetIndex)
                    item.color = "transparent"
                    item.borderColor = "transparent"
                }
                columnModel.setProperty(index, "targetIndex", targetIndex)
            }

            /**
             * Randomize array element order in-place.
             * Using Durstenfeld shuffle algorithm.
             */
            function shuffleArray(a) {
                for (var i = a.length - 1; i > 0; i--) {
                    var j = Math.floor(Math.random() * (i + 1));
                    var temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
        }

        Button {
            Layout.fillWidth: true
            text: qsTr("Submit")
        }
    }
}
