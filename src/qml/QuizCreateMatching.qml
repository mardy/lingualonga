import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ScrollView {
    id: root

    property var actions: []
    property var quizData: ({})

    anchors.fill: parent
    contentWidth: width

    property var _matches: quizData.matches ?
        quizData.matches : [_emptyMatch, _emptyMatch]
    property var _emptyMatch: ({
        "text1": "",
        "text2": "",
    })

    ColumnLayout {
        anchors { fill: parent; leftMargin: 8; rightMargin: 8 }

        Label {
            Layout.fillWidth: true
            text: qsTr("Question text:")
        }
        TextArea {
            id: questionControl
            Layout.fillWidth: true
            text: quizData.question ? quizData.question : ""
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Enter the matches, one per row. Shuffling will happen later.")
            wrapMode: Text.WordWrap
        }

        Repeater {
            id: answerRepeater
            model: root._matches

            RowLayout {
                Layout.fillWidth: true
                spacing: 12

                property alias text1: field1.text
                property alias text2: field2.text
                property bool empty: !text1 && !text2
                property alias firstField: field1

                TextField {
                    id: field1
                    Layout.fillWidth: true
                    text: modelData.text1
                }

                TextField {
                    id: field2
                    Layout.fillWidth: true
                    text: modelData.text2
                }
            }

            function matches() {
                var answers = []
                for (var i = 0; i < answerRepeater.count; i++) {
                    var item = answerRepeater.itemAt(i)
                    var text1 = item.text1.trim()
                    var text2 = item.text2.trim()
                    if (!text1 && !text2) continue
                    var answer = {
                        "text1": item.text1,
                        "text2": item.text2,
                    }
                    answers.push(answer)
                }
                console.log("Saved answers: " + JSON.stringify(answers))
                return answers
            }
        }

        Button {
            Layout.fillWidth: true
            text: qsTr("Add a row")
            enabled: answerRepeater.count == 0 ||
                !answerRepeater.itemAt(answerRepeater.count - 1).empty
            onClicked: {
                var matches = answerRepeater.matches()
                matches.push(root._emptyMatch)
                root._matches = matches

                /* Avoid jumping back to the top of the page */
                var flickable = root.contentItem
                var buttonBottom = y + height
                if (buttonBottom > flickable.contentY + flickable.height) {
                    flickable.contentY = buttonBottom - flickable.height
                }

                answerRepeater.itemAt(answerRepeater.count - 1).firstField.forceActiveFocus()
            }
        }
    }

    function save() {
        var data = {}
        data.question = questionControl.text
        data.matches = answerRepeater.matches()
        root.quizData = data
        return true
    }
}
