import QtQuick.Controls 2.12
import it.mardy.lingualonga 1.0

Button {
    id: root

    property string coursePath: ""
    property string lessonId: ""

    property var _lesson: Utils.parseJson(fileIO.contents, {})

    flat: true
    text: qsTr("%1 - %2").arg(index + 1).arg(_lesson.title)
    onClicked: {
        var item = stackView.push(Qt.resolvedUrl("LessonEditor.qml"), {
            "coursePath": root.coursePath,
            "lessonId": root.lessonId,
        })
        item.saved.connect(function() {
            // the title might have been changed, force a reload
            fileIO.reload()
        })
    }

    FileIO {
        id: fileIO
        filePath: root.coursePath + "/" + root.lessonId + ".ll"
    }
}

