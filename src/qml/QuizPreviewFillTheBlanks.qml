import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ScrollView {
    id: root

    property var quizData: ({})

    anchors.fill: parent
    contentWidth: width

    ColumnLayout {
        anchors { fill: parent; leftMargin: 8; rightMargin: 8 }

        FillableText {
            id: fillable
            Layout.fillWidth: true
            fillableText: root.setupBlanksInText(quizData.fillableText,
                                                 quizData.blanks)

            Repeater {
                model: fillable.fillableAreas
                delegate: MouseArea {
                    property int padding: 2
                    property bool editing: false
                    x: modelData.x - padding
                    y: modelData.y - padding
                    width: modelData.width + 2 * padding
                    height: modelData.height + 2 * padding
                    onPressed: {
                        editing = true
                        textField.forceActiveFocus()
                    }

                    Rectangle {
                        anchors { fill: parent; margins: parent.padding }
                        color: "transparent"
                        border { width: 2; color: "#ccc" }
                        visible: !label.visible && !textField.visible
                    }

                    Label {
                        id: label
                        anchors.centerIn: parent
                        padding: parent.padding
                        visible: text && !textField.visible
                        color: "blue"
                        font.pixelSize: fillable.font.pixelSize
                        font.family: "Corsivo"
                        text: textField.text
                    }

                    TextField {
                        id: textField
                        anchors {
                            left: parent.left; right: parent.right
                            baseline: label.baseline
                        }
                        padding: parent.padding
                        visible: parent.editing
                        inputMethodHints: Qt.ImhNoAutoUppercase
                        onEditingFinished: parent.editing = false
                    }
                }
            }
        }

        Button {
            Layout.fillWidth: true
            text: qsTr("Submit")
        }
    }

    function setupBlanksInText(text, blanks) {
        if (!text) return ""
        var ret = ""
        var blankStart = -1
        var blankEnd = 0
        for (var i = 0; i < blanks.length; i++) {
            var blank = blanks[i]
            ret += text.substring(blankEnd, blank.start)
            var characterCount = blank.end - blank.start
            // add a couple of characters, and round up the number to the
            // nearest multiple of 5
            var blankLength = Math.ceil((characterCount + 2) / 5) * 5
            ret += "_".repeat(blankLength)
            blankEnd = blank.end
        }
        ret += text.substring(blankEnd)
        return ret
    }
}
