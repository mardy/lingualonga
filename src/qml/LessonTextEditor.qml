import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.lingualonga 1.0

Page {
    id: root

    property string lessonText: ""

    signal saved()

    title: qsTr("Edit lesson text")
    Component.onCompleted: textArea.forceActiveFocus()

    header: ToolBar {
        RowLayout {
            anchors { fill: parent; leftMargin: 8; rightMargin: 8 }
            ToolButton {
                Layout.alignment: Qt.AlignRight
                text: "\uE800" // icon-ok
                font.family: "fontello"
                onClicked: {
                    lessonText = textArea.text
                    root.saved()
                    stackView.pop()
                }
            }
        }
    }

    footer: ToolBar {
        Flickable {
            anchors.fill: parent
            contentWidth: toolRow.implicitWidth
            flickableDirection: Qt.Horizontal
            boundsBehavior: Flickable.StopAtBounds

            Row {
                id: toolRow
                ToolButton {
                    id: boldButton
                    text: "\uE801" // icon-bold
                    font.family: "fontello"
                    // Don't want to close the virtual keyboard when this is clicked.
                    focusPolicy: Qt.NoFocus
                    enabled: textArea.hasSelection
                    checkable: true
                    checked: document.bold
                    onClicked: document.bold = !document.bold
                }
                ToolButton {
                    id: italicButton
                    text: "\uE802" // icon-italic
                    font.family: "fontello"
                    focusPolicy: Qt.NoFocus
                    enabled: textArea.hasSelection
                    checkable: true
                    checked: document.italic
                    onClicked: document.italic = !document.italic
                }
                ToolButton {
                    id: underlineButton
                    text: "\uF0CD" // icon-underline
                    font.family: "fontello"
                    focusPolicy: Qt.NoFocus
                    enabled: textArea.hasSelection
                    checkable: true
                    checked: document.underline
                    onClicked: document.underline = !document.underline
                }

                ToolSeparator {}

                ToolButton {
                    id: alignLeftButton
                    text: "\uE803" // icon-align-left
                    font.family: "fontello"
                    focusPolicy: Qt.NoFocus
                    checkable: true
                    checked: document.alignment == Qt.AlignLeft
                    onClicked: document.alignment = Qt.AlignLeft
                }
                ToolButton {
                    id: alignCenterButton
                    text: "\uE804" // icon-align-center
                    font.family: "fontello"
                    focusPolicy: Qt.NoFocus
                    checkable: true
                    checked: document.alignment == Qt.AlignHCenter
                    onClicked: document.alignment = Qt.AlignHCenter
                }
                ToolButton {
                    id: alignRightButton
                    text: "\uE805" // icon-align-right
                    font.family: "fontello"
                    focusPolicy: Qt.NoFocus
                    checkable: true
                    checked: document.alignment == Qt.AlignRight
                    onClicked: document.alignment = Qt.AlignRight
                }
                ToolButton {
                    id: alignJustifyButton
                    text: "\uE806" // icon-align-justify
                    font.family: "fontello"
                    focusPolicy: Qt.NoFocus
                    checkable: true
                    checked: document.alignment == Qt.AlignJustify
                    onClicked: document.alignment = Qt.AlignJustify
                }
            }
        }
    }

    /* Using a ScrollView here causes the TextArea to be resized to its
     * implicit size at random times. */
    Flickable {
        id: scrollview
        anchors.fill: parent
        contentWidth: scrollview.width
        contentHeight: textArea.implicitHeight
        ScrollBar.vertical: ScrollBar { active: scrollview.moving || scrollview.repositioning }
        property bool repositioning: false

        TextArea {
            id: textArea
            property bool hasSelection: selectionStart != selectionEnd
            anchors.fill: parent
            placeholderText: qsTr("Enter the lesson text")
            textFormat: Qt.RichText
            text: root.lessonText
            wrapMode: TextArea.Wrap
            leftPadding: 8
            rightPadding: 8
            selectByMouse: true
            onCursorRectangleChanged: scrollview.ensureVisible(cursorRectangle)
        }

        Timer {
            id: repositioningTimer
            interval: 50
            repeat: false
            onTriggered: scrollview.repositioning = false
        }

        function reposition(y) {
            contentY = y
            repositioning = true
            repositioningTimer.restart()
        }

        function ensureVisible(rect) {
            if (contentY > rect.y) {
                reposition(rect.y)
            } else if (contentY + height < rect.y + rect.height) {
                reposition(contentY = rect.y + rect.height - height)
            }
        }
    }

    DocumentHandler {
        id: document
        document: textArea.textDocument
        cursorPosition: textArea.cursorPosition
        selectionStart: textArea.selectionStart
        selectionEnd: textArea.selectionEnd
        // textColor: TODO
    }
}
