import QtQuick 2.12
import QtQuick.Controls 2.12

Label {
    id: root

    property alias color: rect.color
    property var borderColor: "transparent"

    padding: 2
    background: Rectangle {
        id: rect
        color: "transparent"
        border { color: root.borderColor; width: 2 }
    }
}
