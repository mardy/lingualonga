import QtQuick 2.12
import QtQuick.Controls 2.12

Item {
    id: root

    property string fillableText: ""
    readonly property var fillableAreas: width, textArea.computeAreas(fillableText)
    property alias font: textArea.font

    implicitHeight: textArea.implicitHeight

    TextArea {
        id: textArea
        anchors.fill: parent
        enabled: false
        wrapMode: Text.WordWrap
        horizontalAlignment: TextEdit.AlignJustify
        textFormat: Text.RichText
        text: formatBlanks(root.fillableText)

        function formatBlanks(fillableText) {
            return fillableText.replace(/_/g, "\u00a0\u00a0")
        }

        function computeAreas(text) {
            if (!textArea.text) return []
            var areaOpen = false
            var areas = []
            var area = {}
            for (var pos = 0; pos < textArea.length; pos++) {
                var character = textArea.getText(pos, pos + 1)
                if (character == '\u00a0') {
                    var r = textArea.positionToRectangle(pos)
                    var r2 = textArea.positionToRectangle(pos + 1)
                    var end
                    if (r2.y == r.y) {
                        // same line, we can use the width data
                        end = r2.x + r2.width
                    } else {
                        end = width
                    }
                    if (!areaOpen) {
                        areaOpen = true
                        area["x"] = r.x
                        area["y"] = r.y
                        area["height"] = r.height
                        area["width"] = end - r.x
                    } else {
                        area["width"] = Math.max(area.x + area.width, end) - area.x
                    }
                } else if (areaOpen) {
                    areas.push(area)
                    area = {}
                    areaOpen = false
                }
            }
            if (areaOpen) {
                areas.push(area)
            }
            return areas
        }
    }
}
