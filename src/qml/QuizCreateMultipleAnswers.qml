import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ScrollView {
    id: root

    property var actions: [ actionPrevious, actionNext, actionNew ]
    property var quizData: ({})

    anchors.fill: parent
    contentWidth: width

    property var _questions: quizData.hasOwnProperty("questions") ? quizData.questions : []
    property var _answers: _quiz.answers
    property int _currentIndex: 0
    property var _quiz: _currentIndex < _questions.length ? _questions[_currentIndex] : _emptyQuestion
    property var _emptyQuestion: ({
        "question": "",
        "answers": [_emptyAnswer, _emptyAnswer],
    })
    property var _emptyAnswer: ({
        "text": "",
        "correct": false,
    })

    on_QuizChanged: {
        root._answers = _quiz.answers.length >= 2 ?
            _quiz.answers : [_emptyAnswer, _emptyAnswer]
    }

    ColumnLayout {
        anchors { fill: parent; leftMargin: 8; rightMargin: 8 }

        Label {
            Layout.fillWidth: true
            text: qsTr("Question text:")
        }
        TextArea {
            id: questionControl
            Layout.fillWidth: true
            text: _quiz.question ? _quiz.question : ""
        }

        Repeater {
            id: answerRepeater
            model: root._answers

            Frame {
                Layout.fillWidth: true

                property alias text: answerText.text
                property alias correct: answerCorrect.checked
                property alias answerField: answerText

                GridLayout {
                    anchors.fill: parent
                    columns: 2

                    TextField {
                        id: answerText
                        Layout.columnSpan: 2
                        Layout.fillWidth: true
                        placeholderText: qsTr("Type an answer")
                        text: modelData.text
                    }
                    CheckBox {
                        id: answerCorrect
                        Layout.fillWidth: true
                        text: qsTr("Correct answer")
                        checked: modelData.correct
                    }
                }
            }

            function saveAnswers() {
                var answers = []
                for (var i = 0; i < answerRepeater.count; i++) {
                    var item = answerRepeater.itemAt(i)
                    var text = item.text.trim()
                    if (!text) continue
                    var answer = {
                        "text": item.text,
                        "correct": item.correct,
                    }
                    answers.push(answer)
                }
                console.log("Saved answers: " + JSON.stringify(answers))
                root._answers = answers
            }
        }

        Button {
            Layout.fillWidth: true
            text: qsTr("Add an answer")
            enabled: answerRepeater.count == 0 ||
                answerRepeater.itemAt(answerRepeater.count - 1).text != ""
            onClicked: {
                answerRepeater.saveAnswers()
                var answers = root._answers
                answers.push(root._emptyAnswer)
                root._answers = answers

                /* Avoid jumping back to the top of the page */
                var flickable = root.contentItem
                var buttonBottom = y + height
                if (buttonBottom > flickable.contentY + flickable.height) {
                    flickable.contentY = buttonBottom - flickable.height
                }

                answerRepeater.itemAt(answerRepeater.count - 1).answerField.forceActiveFocus()
            }
        }
    }

    Action {
        id: actionPrevious
        text: "\u23EE" // previous
        enabled: root._currentIndex > 0
        onTriggered: {
            saveCurrent()
            root._currentIndex--
        }
    }
    Action {
        id: actionNext
        text: "\u23ED" // next
        enabled: root._currentIndex < root._questions.length - 1
        onTriggered: {
            saveCurrent()
            root._currentIndex++
        }
    }
    Action {
        id: actionNew
        text: "+"
        enabled: questionControl.text.length > 0
        onTriggered: {
            saveCurrent()
            root._currentIndex = root._questions.length
        }
    }

    function saveCurrent() {
        var data = {}
        data.question = questionControl.text
        answerRepeater.saveAnswers()
        data.answers = root._answers
        root._questions[root._currentIndex] = data
    }

    function save() {
        saveCurrent()
        root.quizData = { "questions": root._questions }
        return true
    }
}
