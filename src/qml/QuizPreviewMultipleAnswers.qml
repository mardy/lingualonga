import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

ScrollView {
    id: root

    property var quizData: null

    contentWidth: width

    ColumnLayout {
        anchors { fill: parent; leftMargin: 8; rightMargin: 8 }

        Repeater {
            model: quizData.questions

            ColumnLayout {
                Layout.fillWidth: true
                Label {
                    Layout.fillWidth: true
                    text: modelData.question
                }

                Repeater {
                    model: modelData.answers

                    CheckBox {
                        Layout.fillWidth: true
                        Layout.leftMargin: 12
                        text: modelData.text
                    }
                }
            }
        }

        Button {
            Layout.fillWidth: true
            text: qsTr("Submit")
        }
    }
}
