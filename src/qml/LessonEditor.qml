import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.lingualonga 1.0
import "quiz_types.js" as QuizTypes

Page {
    id: root
    title: qsTr("Create lesson")

    // directory where the lesson is (or will be) saved
    property string coursePath: ""

    // basename for the lesson file (minus the ".ll" extension)
    property string lessonId: ""

    property string lessonText: ""
    property var preQuizzes: []
    property var quizzes: []

    signal saved()

    Component.onCompleted: {
        var lesson = Utils.parseJson(storage.contents, {})
        preQuizzes = lesson.preQuizzes || []
        lessonText = lesson.lessonText || ""
        quizzes = lesson.quizzes || []
        if (lesson.title) {
            titleControl.text = lesson.title
        }
    }

    ScrollView {
        id: scrollview
        anchors.fill: parent
        contentWidth: scrollview.width

        ColumnLayout {
            id: contents
            anchors { left: parent.left; right: parent.right; top: parent.top; margins: 12 }

            Label {
                Layout.fillWidth: true
                text: qsTr("Lesson title:")
            }

            TextField {
                id: titleControl
                Layout.fillWidth: true
                placeholderText: qsTr("Enter a title for the lesson")
            }

            QuizListEditable {
                Layout.fillWidth: true
                title: qsTr("Pre-lesson exercises")
                model: preQuizzes
            }

            ColumnLayout {
                Layout.alignment: Qt.AlignHCenter
                Layout.fillWidth: true
                Layout.maximumWidth: implicitWidth + 40

                Button {
                    Layout.fillWidth: true
                    text: qsTr("Enter the lesson text")
                    onClicked: {
                        var item = stackView.push(Qt.resolvedUrl("LessonTextEditor.qml"), {
                            "lessonText": root.lessonText,
                        })
                        item.saved.connect(function () {
                            root.lessonText = item.lessonText
                        })
                    }
                }
            }

            QuizListEditable {
                Layout.fillWidth: true
                title: qsTr("Exercises")
                model: quizzes
            }

            Button {
                Layout.fillWidth: true
                Layout.bottomMargin: 24 // compensate the parent's margins
                text: qsTr("Save")
                onClicked: { root.save(); stackView.pop(); }
            }

            FileIO {
                id: storage
                filePath: root.coursePath + "/" + root.lessonId + ".ll"
            }
        }
    }

    function save() {
        var title = titleControl.text
        if (!root.lessonId) {
            root.lessonId = Utils.uniqueBaseName(root.coursePath, title, ".ll")
        }

        var lesson = {
            "title": title,
            "preQuizzes": root.preQuizzes,
            "lessonText": root.lessonText,
            "quizzes": root.quizzes,
        }
        storage.contents = JSON.stringify(lesson, null, 2)
        root.saved()
    }
}
