import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "quiz_types.js" as QuizTypes

GroupBox {
    id: root
    property var model: []

    ColumnLayout {
        anchors.fill: parent

        Repeater {
            id: repeater
            model: root.model
            Button {
                property var typeData: QuizTypes.propertiesForType(modelData.type)
                text: qsTr("#%1 - %2").arg(index + 1).arg(typeData.name)
                flat: true
                onClicked: {
                    var properties = typeData
                    properties.quizData = modelData.quizData
                    var item = stackView.push(Qt.resolvedUrl("QuizEditor.qml"), properties)
                    item.saved.connect(function() {
                        root.updatePreQuiz(index, item.quizData)
                    })
                }
            }
        }

        Label {
            Layout.fillWidth: true
            horizontalAlignment: Qt.AlignHCenter
            text: qsTr("No exercises")
            visible: root.model.length == 0
        }

        Button {
            Layout.fillWidth: true
            text: qsTr("Add an exercise")
            onClicked: {
                var item = stackView.push(Qt.resolvedUrl("QuizChooser.qml"), {
                })
                item.created.connect(root.prependQuiz)
            }
        }
    }

    function prependQuiz(quizData) {
        var tmp = model
        tmp.push(quizData)
        model = tmp
    }

    function updatePreQuiz(index, quizData) {
        var tmp = model
        tmp[index].quizData = quizData
        model = tmp
    }
}

