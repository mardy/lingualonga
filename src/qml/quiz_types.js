var model = [
    {
        name: qsTr("Multiple answers"),
        tag: "multiple-answers",
        editComponent: "QuizCreateMultipleAnswers.qml",
        previewComponent: "QuizPreviewMultipleAnswers.qml",
    },
    {
        name: qsTr("Matching"),
        tag: "matching",
        editComponent: "QuizCreateMatching.qml",
        previewComponent: "QuizPreviewMatching.qml",
    },
    {
        name: qsTr("Fill the blanks"),
        tag: "fill-the-blanks",
        editComponent: "QuizCreateFillTheBlanks.qml",
        previewComponent: "QuizPreviewFillTheBlanks.qml",
    }
]

function propertiesForType(tag) {
    for (var i = 0; i < model.length; i++) {
        var o = model[i]
        if (o.tag == tag) return o
    }
    return null
}
