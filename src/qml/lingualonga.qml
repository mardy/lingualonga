import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.lingualonga 1.0

ApplicationWindow {
    id: root

    width: 480
    height: 320
    visible: true
    title: qsTr("LinguaLonga")

    StackView {
        id: stackView
        anchors { left: parent.left; right: parent.right; top: parent.top; bottom: osk.top }
        initialItem: dummyPage
        Keys.onBackPressed: if (depth > 1) {
            event.accepted = true
            stackView.pop()
        } else {
            event.accepted = false
        }
    }

    Component {
        id: dummyPage
        Page {
            ColumnLayout {
                anchors.centerIn: parent

                Label {
                    text: "Hello world"
                }

                Button {
                    text: "Next!"
                    onClicked: stackView.push(Qt.resolvedUrl("CourseEditor.qml"), {
                        "parentPath": standardPaths.filePath("courses"),
                    })
                    //onClicked: stackView.push(Qt.resolvedUrl("LessonEditor.qml"))
                }

                StandardPaths {
                    id: standardPaths
                    base: StandardPaths.AppDataLocation
                }
            }
        }
    }

    KeyboardRectangle { id: osk }
}
