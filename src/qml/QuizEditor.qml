import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12

Page {
    id: root

    property string name: ""
    property string editComponent: ""
    property string previewComponent: ""
    property var quizData: null

    signal saved()

    title: qsTr("Create quiz: %1").arg(name)

    header: ToolBar {
        RowLayout {
            anchors { fill: parent; leftMargin: 8; rightMargin: 8 }

            Repeater {
                model: flipable.front.item.actions
                ToolButton {
                    action: modelData
                    enabled: action.enabled && !flipable.inPreview
                }
            }

            Item {
                Layout.fillWidth: true
            }
            ToolButton {
                Layout.alignment: Qt.AlignRight
                text: "\u23EF" // play/pause
                onClicked: {
                    flipable.front.item.save()
                    flipable.inPreview = !flipable.inPreview
                }
            }
            ToolButton {
                Layout.alignment: Qt.AlignRight
                text: "\uE800" // icon-ok
                font.family: "fontello"
                onClicked: {
                    if (flipable.front.item.save()) {
                        root.quizData = flipable.front.item.quizData
                        root.saved()
                        stackView.pop()
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        flipable.front.setSource(Qt.resolvedUrl(editComponent), {
            "quizData": Qt.binding(function() { return root.quizData }),
        })
        flipable.back.setSource(Qt.resolvedUrl(previewComponent), {
            "quizData": Qt.binding(function() { return flipable.front.item.quizData }),
        })
    }

    Flipable {
        id: flipable

        property bool inPreview: false
        anchors.fill: parent
        front: Loader {
            anchors.fill: parent
        }
        back: Loader {
            anchors.fill: parent
        }

        transform: Rotation {
            id: rotation
            origin.x: flipable.width / 2
            origin.y: flipable.height / 2
            axis.x: 0; axis.y: 1; axis.z: 0
            angle: 0
        }

        states: State {
            name: "back"
            PropertyChanges { target: rotation; angle: 180 }
            when: flipable.inPreview
        }

        transitions: Transition {
            NumberAnimation { target: rotation; property: "angle"; duration: 400 }
        }

    }
}
