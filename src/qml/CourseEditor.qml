import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import it.mardy.lingualonga 1.0

Page {
    id: root
    title: qsTr("Create course")

    // Path inside which the course is located (or will be placed)
    property string parentPath: ""
    // Path for the course (once saved)
    property string coursePath: parentPath + "/" + courseId + ".llc/"
    property string courseId: ""
    property string realPath: courseId ? coursePath : tmpDir.path
    property var lessons: []
    property var courses: []

    signal saved()

    Component.onCompleted: {
        if (courseId) {
            var course = Utils.parseJson(storage.contents, {})
            lessons = course.lessons || []
            courses = course.courses || []
            if (course.title) {
                titleControl.text = course.title
            }
        }
    }

    ScrollView {
        id: scrollview
        anchors.fill: parent
        contentWidth: scrollview.width

        ColumnLayout {
            id: contents
            anchors { left: parent.left; right: parent.right; top: parent.top; margins: 12 }

            Label {
                Layout.fillWidth: true
                text: qsTr("Course title:")
            }

            TextField {
                id: titleControl
                Layout.fillWidth: true
                placeholderText: qsTr("Enter a name for the course")
            }

            Repeater {
                model: root.lessons

                LessonButtonDelegate {
                    Layout.fillWidth: true
                    coursePath: root.realPath
                    lessonId: modelData
                }
            }

            Repeater {
                model: root.courses

                CourseButtonDelegate {
                    Layout.fillWidth: true
                    parentPath: root.realPath
                    courseId: modelData
                }
            }

            Button {
                Layout.fillWidth: true
                Layout.margins: 24
                text: qsTr("Add a lesson")
                visible: root.courses.length == 0
                onClicked: {
                    var item = stackView.push(Qt.resolvedUrl("LessonEditor.qml"), {
                        "coursePath": root.realPath,
                    })
                    item.saved.connect(function() {
                        var tmp = root.lessons
                        tmp.push(item.lessonId)
                        root.lessons = tmp
                    })
                }
            }

            Button {
                Layout.fillWidth: true
                Layout.margins: 24
                text: qsTr("Add a sub-course")
                visible: root.lessons.length == 0
                onClicked: {
                    var item = stackView.push(Qt.resolvedUrl("CourseEditor.qml"), {
                        "parentPath": root.realPath,
                    })
                    item.saved.connect(function() {
                        var tmp = root.courses
                        tmp.push(item.courseId)
                        root.courses = tmp
                    })
                }
            }

            Button {
                Layout.fillWidth: true
                Layout.bottomMargin: 24 // compensate the parent's margins
                enabled: titleControl.text
                text: qsTr("Save")
                onClicked: { root.save(); stackView.pop(); }
            }

            FileIO {
                id: storage
                filePath: root.realPath + "/index.ll"
            }

            TemporaryDir {
                id: tmpDir
                parentPath: root.parentPath
            }
        }
    }

    function save() {
        var title = titleControl.text

        if (!root.courseId) {
            /* Don't use root.courseId as that would also cause
             * root.realPath to change. We don't want that to
             * happen before having called renameDirectory. */
            var courseIdLocal = Utils.uniqueBaseName(root.parentPath, title, ".llc")
            Utils.renameDirectory(root.realPath, courseIdLocal + ".llc")
            root.courseId = courseIdLocal
        }

        var course = {
            "title": title,
            "courses": root.courses,
            "lessons": root.lessons,
        }
        storage.contents = JSON.stringify(course, null, 2)
        root.saved()
    }
}
