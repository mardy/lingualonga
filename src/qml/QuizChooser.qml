import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.Layouts 1.12
import "quiz_types.js" as QuizTypes

Page {
    id: root

    property var quizData: ({})

    signal created(var quizData)

    title: qsTr("Choose a quiz type")

    ScrollView {
        id: scrollview
        anchors.fill: parent
        contentWidth: scrollview.width

        ListView {
            id: contents

            property int buttonWidth: Math.max(width / 2, 200)

            anchors.fill: parent
            model: QuizTypes.model
            delegate: RowLayout {
                width: ListView.view.width
                Button {
                    Layout.preferredWidth: contents.buttonWidth
                    Layout.alignment: Qt.AlignHCenter
                    text: modelData.name
                    onClicked: {
                        var item = stackView.push(Qt.resolvedUrl("QuizEditor.qml"), {
                            "name": modelData.name,
                            "editComponent": modelData.editComponent,
                            "previewComponent": modelData.previewComponent,
                            "quizData": root.quizData,
                        })
                        item.saved.connect(function() {
                            var quizData = {
                                "quizData": item.quizData,
                                "type": modelData.tag,
                            }
                            root.created(quizData)
                            stackView.pop()
                        })
                    }
                }
            }
        }
    }
}
