/*
 * Copyright (C) 2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef LINGUALONGA_UTILS_H
#define LINGUALONGA_UTILS_H

#include <QByteArray>
#include <QJsonValue>
#include <QObject>
#include <QString>

namespace LinguaLonga {

class Utils: public QObject
{
    Q_OBJECT

public:
    Utils(QObject *parent = 0);
    ~Utils() = default;

    Q_INVOKABLE QString uniqueBaseName(const QString &path,
                                       const QString &title,
                                       const QString &extension) const;

    Q_INVOKABLE QJsonValue parseJson(const QByteArray &json,
        const QJsonValue &defaultValue = QJsonValue()) const;

    Q_INVOKABLE bool renameDirectory(const QString &path,
                                     const QString &name) const;
};

} // namespace

#endif // LINGUALONGA_UTILS_H
