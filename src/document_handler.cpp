/*
 * Copyright (C) 2017 The Qt Company Ltd.
 * Copyright (C) 2019-2020 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of LinguaLonga.
 *
 * LinguaLonga is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LinguaLonga is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LinguaLonga.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "document_handler.h"

#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QFileSelector>
#include <QFont>
#include <QQmlFile>
#include <QQmlFileSelector>
#include <QQuickTextDocument>
#include <QTextCharFormat>
#include <QTextCodec>
#include <QTextCursor>
#include <QTextDocument>

using namespace LinguaLonga;

namespace LinguaLonga {

class DocumentHandlerPrivate
{
    Q_DECLARE_PUBLIC(DocumentHandler)

public:
    DocumentHandlerPrivate(DocumentHandler *q);

    void reset();
    QTextCursor textCursor() const;
    QTextDocument *textDocument() const;
    void mergeFormatOnWordOrSelection(const QTextCharFormat &format);

private:
    QQuickTextDocument *m_document;

    int m_cursorPosition;
    int m_selectionStart;
    int m_selectionEnd;

    QFont m_font;
    int m_fontSize;
    QUrl m_fileUrl;

    DocumentHandler *q_ptr;
};

} // namespace

DocumentHandlerPrivate::DocumentHandlerPrivate(DocumentHandler *q):
    m_document(nullptr),
    m_cursorPosition(-1),
    m_selectionStart(0),
    m_selectionEnd(0),
    q_ptr(q)
{
}

void DocumentHandlerPrivate::reset()
{
    Q_Q(DocumentHandler);
    Q_EMIT q->fontFamilyChanged();
    Q_EMIT q->alignmentChanged();
    Q_EMIT q->boldChanged();
    Q_EMIT q->italicChanged();
    Q_EMIT q->underlineChanged();
    Q_EMIT q->fontSizeChanged();
    Q_EMIT q->textColorChanged();
}

QTextCursor DocumentHandlerPrivate::textCursor() const
{
    QTextDocument *doc = textDocument();
    if (!doc)
        return QTextCursor();

    QTextCursor cursor = QTextCursor(doc);
    if (m_selectionStart != m_selectionEnd) {
        cursor.setPosition(m_selectionStart);
        cursor.setPosition(m_selectionEnd, QTextCursor::KeepAnchor);
    } else {
        cursor.setPosition(m_cursorPosition);
    }
    return cursor;
}

QTextDocument *DocumentHandlerPrivate::textDocument() const
{
    if (!m_document)
        return nullptr;

    return m_document->textDocument();
}

void DocumentHandlerPrivate::mergeFormatOnWordOrSelection(const QTextCharFormat &format)
{
    QTextCursor cursor = textCursor();
    if (!cursor.hasSelection()) {
        return;
    }
    cursor.mergeCharFormat(format);
}

DocumentHandler::DocumentHandler(QObject *parent):
    QObject(parent),
    d_ptr(new DocumentHandlerPrivate(this))
{
}

DocumentHandler::~DocumentHandler()
{
}

QQuickTextDocument *DocumentHandler::document() const
{
    Q_D(const DocumentHandler);
    return d->m_document;
}

void DocumentHandler::setDocument(QQuickTextDocument *document)
{
    Q_D(DocumentHandler);
    if (document == d->m_document)
        return;

    d->m_document = document;
    Q_EMIT documentChanged();
}

int DocumentHandler::cursorPosition() const
{
    Q_D(const DocumentHandler);
    return d->m_cursorPosition;
}

void DocumentHandler::setCursorPosition(int position)
{
    Q_D(DocumentHandler);
    if (position == d->m_cursorPosition)
        return;

    d->m_cursorPosition = position;
    d->reset();
    Q_EMIT cursorPositionChanged();
}

int DocumentHandler::selectionStart() const
{
    Q_D(const DocumentHandler);
    return d->m_selectionStart;
}

void DocumentHandler::setSelectionStart(int position)
{
    Q_D(DocumentHandler);
    if (position == d->m_selectionStart)
        return;

    d->m_selectionStart = position;
    Q_EMIT selectionStartChanged();
}

int DocumentHandler::selectionEnd() const
{
    Q_D(const DocumentHandler);
    return d->m_selectionEnd;
}

void DocumentHandler::setSelectionEnd(int position)
{
    Q_D(DocumentHandler);
    if (position == d->m_selectionEnd)
        return;

    d->m_selectionEnd = position;
    Q_EMIT selectionEndChanged();
}

QString DocumentHandler::fontFamily() const
{
    Q_D(const DocumentHandler);
    QTextCursor cursor = d->textCursor();
    if (cursor.isNull())
        return QString();
    QTextCharFormat format = cursor.charFormat();
    return format.font().family();
}

void DocumentHandler::setFontFamily(const QString &family)
{
    Q_D(DocumentHandler);
    QTextCharFormat format;
    format.setFontFamily(family);
    d->mergeFormatOnWordOrSelection(format);
    Q_EMIT fontFamilyChanged();
}

QColor DocumentHandler::textColor() const
{
    Q_D(const DocumentHandler);
    QTextCursor cursor = d->textCursor();
    if (cursor.isNull())
        return QColor(Qt::black);
    QTextCharFormat format = cursor.charFormat();
    return format.foreground().color();
}

void DocumentHandler::setTextColor(const QColor &color)
{
    Q_D(DocumentHandler);
    QTextCharFormat format;
    format.setForeground(QBrush(color));
    d->mergeFormatOnWordOrSelection(format);
    Q_EMIT textColorChanged();
}

Qt::Alignment DocumentHandler::alignment() const
{
    Q_D(const DocumentHandler);
    QTextCursor cursor = d->textCursor();
    if (cursor.isNull())
        return Qt::AlignLeft;
    return d->textCursor().blockFormat().alignment();
}

void DocumentHandler::setAlignment(Qt::Alignment alignment)
{
    Q_D(DocumentHandler);
    QTextBlockFormat format;
    format.setAlignment(alignment);
    QTextCursor cursor = d->textCursor();
    cursor.mergeBlockFormat(format);
    Q_EMIT alignmentChanged();
}

bool DocumentHandler::bold() const
{
    Q_D(const DocumentHandler);
    QTextCursor cursor = d->textCursor();
    if (cursor.isNull())
        return false;
    return d->textCursor().charFormat().fontWeight() == QFont::Bold;
}

void DocumentHandler::setBold(bool bold)
{
    Q_D(DocumentHandler);
    QTextCharFormat format;
    format.setFontWeight(bold ? QFont::Bold : QFont::Normal);
    d->mergeFormatOnWordOrSelection(format);
    Q_EMIT boldChanged();
}

bool DocumentHandler::italic() const
{
    Q_D(const DocumentHandler);
    QTextCursor cursor = d->textCursor();
    if (cursor.isNull())
        return false;
    return d->textCursor().charFormat().fontItalic();
}

void DocumentHandler::setItalic(bool italic)
{
    Q_D(DocumentHandler);
    QTextCharFormat format;
    format.setFontItalic(italic);
    d->mergeFormatOnWordOrSelection(format);
    Q_EMIT italicChanged();
}

bool DocumentHandler::underline() const
{
    Q_D(const DocumentHandler);
    QTextCursor cursor = d->textCursor();
    if (cursor.isNull())
        return false;
    return d->textCursor().charFormat().fontUnderline();
}

void DocumentHandler::setUnderline(bool underline)
{
    Q_D(DocumentHandler);
    QTextCharFormat format;
    format.setFontUnderline(underline);
    d->mergeFormatOnWordOrSelection(format);
    Q_EMIT underlineChanged();
}

int DocumentHandler::fontSize() const
{
    Q_D(const DocumentHandler);
    QTextCursor cursor = d->textCursor();
    if (cursor.isNull())
        return 0;
    QTextCharFormat format = cursor.charFormat();
    return format.font().pointSize();
}

void DocumentHandler::setFontSize(int size)
{
    Q_D(DocumentHandler);

    if (size <= 0)
        return;

    QTextCursor cursor = d->textCursor();
    if (cursor.isNull())
        return;

    if (!cursor.hasSelection())
        cursor.select(QTextCursor::WordUnderCursor);

    if (cursor.charFormat().property(QTextFormat::FontPointSize).toInt() == size)
        return;

    QTextCharFormat format;
    format.setFontPointSize(size);
    d->mergeFormatOnWordOrSelection(format);
    Q_EMIT fontSizeChanged();
}

QString DocumentHandler::fileName() const
{
    Q_D(const DocumentHandler);
    const QString filePath = QQmlFile::urlToLocalFileOrQrc(d->m_fileUrl);
    const QString fileName = QFileInfo(filePath).fileName();
    if (fileName.isEmpty())
        return QStringLiteral("untitled.txt");
    return fileName;
}

QString DocumentHandler::fileType() const
{
    return QFileInfo(fileName()).suffix();
}

QUrl DocumentHandler::fileUrl() const
{
    Q_D(const DocumentHandler);
    return d->m_fileUrl;
}

void DocumentHandler::load(const QUrl &fileUrl)
{
    Q_D(DocumentHandler);
    if (fileUrl == d->m_fileUrl)
        return;

    QQmlEngine *engine = qmlEngine(this);
    if (!engine) {
        qWarning() << "load() called before DocumentHandler has QQmlEngine";
        return;
    }

    const QUrl path = QQmlFileSelector::get(engine)->selector()->select(fileUrl);
    const QString fileName = QQmlFile::urlToLocalFileOrQrc(path);
    if (QFile::exists(fileName)) {
        QFile file(fileName);
        if (file.open(QFile::ReadOnly)) {
            QByteArray data = file.readAll();
            QTextCodec *codec = QTextCodec::codecForHtml(data);
            if (QTextDocument *doc = d->textDocument())
                doc->setModified(false);

            Q_EMIT loaded(codec->toUnicode(data));
            d->reset();
        }
    }

    d->m_fileUrl = fileUrl;
    Q_EMIT fileUrlChanged();
}

void DocumentHandler::saveAs(const QUrl &fileUrl)
{
    Q_D(DocumentHandler);
    QTextDocument *doc = d->textDocument();
    if (!doc)
        return;

    const QString filePath = fileUrl.toLocalFile();
    const bool isHtml = QFileInfo(filePath).suffix().contains(QLatin1String("htm"));
    QFile file(filePath);
    if (!file.open(QFile::WriteOnly | QFile::Truncate | (isHtml ? QFile::NotOpen : QFile::Text))) {
        Q_EMIT error(tr("Cannot save: ") + file.errorString());
        return;
    }
    file.write((isHtml ? doc->toHtml() : doc->toPlainText()).toUtf8());
    file.close();

    if (fileUrl == d->m_fileUrl)
        return;

    d->m_fileUrl = fileUrl;
    Q_EMIT fileUrlChanged();
}
